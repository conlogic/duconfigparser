..
 =============================================================================
 Title          : Extensions for :py:mod:`configparser`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-03-19

 Description    : Package overview.
 =============================================================================


===================================
General information for the package
===================================

.. toctree::
   :maxdepth: 1

   readme

   license

   changelog


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
