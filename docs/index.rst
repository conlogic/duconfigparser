..
 =============================================================================
 Title          : Extensions for :py:mod:`configparser`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-03-19

 Description    : Master file for the documentation.
 =============================================================================


==============================================================================
Documentation for ``duconfigparser``
==============================================================================

.. toctree::
   :maxdepth: 2

   overview
   usage

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
