..
 =============================================================================
 Title          : Extensions for :py:mod:`configparser`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-03-19

 Description    : Read me file for the package.
 =============================================================================


.. include:: ../README.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
