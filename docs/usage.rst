..
 =============================================================================
 Title          : Extensions for :py:mod:`configparser`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-01-20

 Description    : Description of the user interface.
 =============================================================================


===================
 The user interface
===================

This package ``duconfigparser`` is used by importing its single module,
:py:mod:`duconfigparser`.

The :py:mod:`duconfigparser` provides a class :py:class:`ConfigParser` that
extends :py:class:`configparser.ConfigParser` of the Standard Library.

.. autoclass:: duconfigparser.ConfigParser

Please note that :py:class:`duconfigparser.ConfigParser` is fully backwards
compatible to :py:class:`configparser.ConfigParser` by using ``None`` for
`list_beg`, `list_sep` and `list_end`, and for `line_cont`, too.  In this
case, the values of the other two extra  keyword arguments `oth_begs` and
`oth_ends` are ignored.

Furthermore, one is not forced to use proper list delimiters by using
whitespace-only arguments for `list_beg` and `list_end`.  In this case a list
value string is identified by using putting the list element string on (an)
extra line(s) below the option and the option value separator.  Using no
proper list delimiters does not allow to represent empty lists as values.

Please note that it is also possible to use a sole ``\n`` as list element
separator.

By the way, a main purpose of `oth_begs` and `oth_ends` is to handle
structured values within list value strings properly.  If one, for instance,
wants to combine the flexible value interpolation provided by package
``duipoldict`` (`duipoldict code`_), one can feed the begin / end delimiters
for its value references as elements to the `oth_begs` / `oth_ends` list
values.  (For more information about the value interpolation provided by
``duipoldict``, please consult the `duipoldict documentation`_.)

By `line_cont` an explicit marker for lines that will be continued by the next
line can be given.  This provides the additional universal line continuation
mechanism already mentioned in the ``README``.  Please note its limitation in
regard of line numbers given when parsing errors occur.

At the moment only the reading of list values is implemented.  This is
achieved by overloading the following two core methods for reading values from
configuration parser objects:

.. automethod:: duconfigparser.ConfigParser.get

.. automethod:: duconfigparser.ConfigParser.items


.. _duipoldict code: https://gitlab.com/conlogic/duipoldict
.. _duipoldict documentation: https://duipoldict.readthedocs.io


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
