..
 =============================================================================
 Title          : Extensions for :py:mod:`configparser`

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-01-20

 Description    : Change log for the package.
 =============================================================================


=========
Changelog
=========


Version 2.3 (2022-01-20)
========================

- Add an universal line continuation mechanism using an explicit string to
  mark the end of lines to be continued.

- Add tests for this universal line continuation mechanism, and update the
  documentation for it.

- Fix documentation glitches found in the ``README`` file.


Version 2.2.1 (2021-12-27)
==========================

- Add support for Python 3.10.


Version 2.2 (2021-10-28)
========================

- Fix a typo in the ``CHANGELOG`` file.

- Allow to handle foreign delimiters by adding new keyword arguments
  `oth_begs` and `oth_ends` to our :py:class:`ConfigParser`.  Now only list
  separator occurrences outside of such delimiters are considered as valid
  list separator occurrences.

- Add tests for the new `oth_begs` and `oth_ends` parameters, and update the
  documentation for them.

- Remove redundant license information.


Version 2.1 (2021-10-06)
========================

- Add many more tests:

  - Missing :py:meth:`get` tests for list values without list value
    separators.

  - Tests for :py:meth:`get` for list values without list value separators and
    without proper list element separator.

  - All tests for :py:meth:`items`.

- Make code to match a list value string more modular, and optimize it a bit
  for performance

- Document several choices for the list-value-specific parameters more
  explicitly.


Version 2.1dev1 (2021-09-30)
============================

- Add package ``dubasiclog`` (version >= 1.3) as dependency.

- Add log messages to signal the kind of list value support.

- Add new error :py:exc:`MissingListDelimError` that is raised, if a proper
  list begin or end delimiter is missing.

- Refactor tests for simplicity.  For now only the :py:class:`get` tests are
  redone, but the :py:meth:`items` will follow.


Version 2.0 (2021-09-12)
========================

- Rename main class :py:class:`DUConfigparser` to :py:class:`ConfigParser`.


Version 1.1 (2021-07-05)
========================

- Fix `.gitignore`.

- Restore compatibility to old API.


Version 1.0.2 (2021-07-04)
==========================

- Use more :py:mod:`pathlib` in the package tools.

- Drop support for Python 3.5, since ``f"..."``-strings are used (via
  ``classifiers`` for ``setup``).

- Fix development status (via ``classifiers`` for ``setup``).


Version 1.0.1 (2021-05-21)
==========================

- Fix whitespace handling for list values.


Version 1.0 (2021-05-20)
========================

- Use delimiters to enclose list values.


Version 0.4.1 (2021-05-12)
==========================

- Migrate package tools to ``f"..."``-strings and :py:mod:`pathlib`.


Version 0.4 (2021-05-10)
========================

- Use ``f"..."`` strings instead of ``"...".format(...)``.


Version 0.3 (2021-03-19)
========================

- Add tests for :py:class:`DUConfigParser` fallback to standard
  `ConfigParser`.

- Fix :py:meth:`_is_list_val_str` for ``None`` a list element separator.

- Add tests for the :py:meth:`get` method using its `fallback` argument, too.

- Allow list values for the `fallback` argument of :py:meth:`get`.


Version 0.2 (2021-03-19)
========================

- Add some tests for the :py:class:`DUConfigParser` methods.

- Fix :py:meth:`_as_list_maybe` for non-list values.

- Add Sphinx-based documentation for the package.


Version 0.1.2 (2021-01-06)
==========================

- Update copyright for 2020.


Version 0.1.1 (2020-21-22)
==========================

- Keep options unchanged.


Version 0.1 (2020-21-21)
========================

- Initial version.

- Support to get option list values and list items as real lists.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
