# ============================================================================
# Title          : Extensions for :py:mod:`configparser`
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-01-20
#
# Description    : See documentation string below.
# ============================================================================

'''
Single module for package ``duconfigparser``.

Own extensions for module :py:mod:`configparser` from Standard Library.
'''


import configparser
import re

import dubasiclog


# Parameters
# ==========

# Version of this package.
version = '2.3'


# Functions and classes
# =====================


# For uniform logging we use the module-global logging facilities from package
# ``dubasiclog``.
set_max_loglevel = dubasiclog.set_max_loglevel
log = dubasiclog.log
enable_debug_log = dubasiclog.enable_debug_log
disable_debug_log = dubasiclog.disable_debug_log
debug_is_enabled = dubasiclog.debug_is_enabled
debug_log = dubasiclog.debug_log


class ConfigParser(configparser.ConfigParser):
    '''
    Variant of standard :py:class:`configparser.ConfigParser` that allows list
    values for its options, too.

    Beside the keyword of :py:class:`configparser.ConfigParser` there are the
    following additional keyword arguments:

    + `list_beg`: a string representing the begin delimiter of a list value;

    + `list_sep`: a string representing the separator for the elements of a
      list value;

    + `list_end`: a string representing the end delimiter of a list value.

    + `oth_begs` / `oth_ends`: list of other begin / end delimiters.  Strings
      that are delimited by these delimiters are considered as atomic values
      that are, for instance, not split into several list elements.

    + `line_cont`: a not-``None`` string to mark the end of a continued line
      causes the next line considered as continuation of that line.  Leading
      whitespace of a continuation line is ignored.

    Whitespace around the list delimiters  and list element separator is
    ignored.  A not-``None`` value for all of these list value-specific
    parameters enables support for list values.

    If list values are supported, and one of `list_beg` or `list_end` is built
    of whitespace only no list delimiters are used.   Then only non-empty list
    values are possible, and a list value is expected to start indented on a
    new line.

    Please note a limitation of the line continuation mechanism based on
    `line_cont` marker:  line numbers in error messages for lines after
    continued lines are systematically to low since a continued line together
    with all its continuation lines are treated as single line.
    '''
    def __init__(self, list_beg='[', list_sep=',', list_end=']',
                 oth_begs=[], oth_ends=[], line_cont=None, **kwargs):
        '''
        Initialize a :py:class:`duconfigparser.ConfigParser`.
        '''
        super().__init__(**kwargs)
        self.list_beg = list_beg
        self.list_sep = list_sep
        self.list_end = list_end
        self.oth_begs = oth_begs
        self.oth_ends = oth_ends
        self.line_cont = line_cont

        # We want to use options unchanged.
        self.optionxform = lambda k: k

        # We also define two regexps related to list values, if list values
        # are enabled: one to match the separator of list elements, and one to
        # match whole list values.
        if None in [self.list_beg, self.list_sep, self.list_end]:
            # No list values are supported.
            log(1, 'Support for list values disabled.')
            self._list_val_supp = False
        else:
            # List values are supported.
            self._list_val_supp = True
            # - Regexp for list value separator.
            list_sep_rx = r'\s*' + re.escape(self.list_sep) + r'\s*'
            self._list_sep_rx = re.compile(list_sep_rx)
            # - Regexp to search for valid list value separators.  Here is
            #   also for searched for other begin / end separators.
            list_sep_search_rx = list_sep_rx
            for d in self.oth_begs:
                list_sep_search_rx += f"|{re.escape(d)}"
            for d in self.oth_ends:
                list_sep_search_rx += f"|{re.escape(d)}"
            self._list_sep_search_rx = re.compile(list_sep_search_rx)
            if '' in [self.list_beg.strip(), self.list_end.strip()]:
                # No list delimiters are used.
                log(1, 'Support for list values without delimiters enabled.')
                self._use_list_delims = False
                # - Regexp for list begin delimiter.
                list_beg_rx = r'^\s*\n'
                # - Regexp for list end delimiter.
                list_end_rx = r'\s*$'
            else:
                # List delimiters are used.
                self._use_list_delims = True
                log(1, 'Support for list values with delimiters enabled.')
                # - Regexp for list begin delimiter.
                list_beg_rx = r'^\s*' + re.escape(self.list_beg)
                # - Regexp for list end delimiter.
                list_end_rx = re.escape(self.list_end) + r'\s*$'
            self._list_beg_rx = re.compile(list_beg_rx, re.DOTALL)
            self._list_end_rx = re.compile(list_end_rx, re.DOTALL)
        # Whole string at the end of a continued line.
        if self.line_cont is not None:
            self._cont_line_end = self.line_cont + '\n'


    def _read(self, fp, fpname):
        '''
        Variant of :py:meth:`configparser.RawConfigParser._read` to merge
        continued lines with their continuation lines into a single lines.
        '''
        if self.line_cont is None:
            # Line continuation is not defined -> do nothing special.
            super()._read(fp, fpname)
        else:
            # Line continuation is defined -> we do the following steps:
            # 1. Read the `fp` lines into a list.
            # 2. Create a new line list from it with continued lines and their
            #    continuation lines merged into a single lines.
            # 3. Feed this new list as iterable via the `fp` argument to the
            #    original :py:meth:`_read`.

            # 1. Read `fp` lines.
            fp_lines = [l for l in fp]

            # 2. Create new line list with continued lines and their
            #    continuation lines merged.
            merged_fp_lines = self._merge_continued_lines(fp_lines)

            # 3. Feed new list to original :py:meth:`_read`.
            super()._read(merged_fp_lines, fpname)


    def _merge_continued_lines(self, lines):
        '''
        Create from list `lines` of lines a new list of lines with continued
        lines and their continuation lines merged into a single lines.

        :return: List with merged lines.
        '''
        merged_lines = []
        # Flag whether line continuation is active, i.e. last line has to be
        # continued.
        act_line_cont = False
        for l in lines:
            # Calculate active line.
            if act_line_cont:
                # Line continuation is active -> active line is previous one,
                # with current one, stripped of leading whitespace, append.
                stripped_l = l.lstrip()
                act_l = merged_lines.pop() + stripped_l
            else:
                # Line continuation is not active -> active line is current
                # one.
                act_l = l
            # Check whether active line needs to be continued.
            if act_l.endswith(self._cont_line_end):
                # Active has a continuation.
                act_line_cont = True
                act_l = act_l[:-len(self._cont_line_end)]
            else:
                # Active line has no continuation.
                act_line_cont = False
            # Add active line.
            merged_lines.append(act_l)

        return merged_lines


    def _list_content_str(self, val_str):
        '''
        :return: the string representing the content of value string `val_str`
                 read as list if `val_str` represents a list value, and
                 ``None`` otherwise.
        '''
        list_beg_matcher = re.match(self._list_beg_rx, val_str)
        list_end_matcher = re.search(self._list_end_rx, val_str)
        # Check whether we have list value string.
        if list_beg_matcher is not None and list_end_matcher is not None:
            # List value found -> extract content string.
            cont_str_ws = val_str[
                list_beg_matcher.end():list_end_matcher.start()]
            cont_str = cont_str_ws.strip()
        elif self._use_list_delims and list_beg_matcher is not None:
            # No list value found, but a list begin delimiter -> missing list
            # end delimiter.
            raise MissingListDelimError(val_str, list_end=self.list_end)
        elif self._use_list_delims and list_end_matcher is not None:
            # No list value found, but a list end delimiter -> missing list
            # begin delimiter.
            raise MissingListDelimError(val_str, list_beg=self.list_beg)
        else:
            # No list value found.
            cont_str = None

        return cont_str


    def _maybe_list_content_str(self, val_str):
        '''
        :return: the string representing the content of value string `val_str`
                 read as list if list values are enabled and `val_str`
                 represents a list value, and ``None`` otherwise.
        '''
        if self._list_val_supp:
            # List value support -> use specialized method.
            cont_str = self._list_content_str(val_str)
        else:
            # No list value support -> no option values are treated as list
            # values.
            cont_str = None

        return cont_str


    def _as_list_maybe(self, val_str):
        '''
        Convert value string `val_str` to a list, if list values are enabled
        and `val_str` is a list value string.

        :return: `val_str` as list, if converted, or stripped otherwise.
        '''
        cont_str = self._maybe_list_content_str(val_str)
        if cont_str is None:
            # A non-list value is used unchanged, but stripped of whitespace.
            val = val_str.strip()
        elif cont_str == '':
            # An empty value should represent an empty list.
            val = []
        else:
            # Otherwise we split at the list element separators.
            val = self._split_list_elems(cont_str)

        return val


    def _split_list_elems(self, cont_str):
        '''
        Split the string `con_str` representing the content of an non-empty
        list into the respective elements.

        :return: list of the list elements given by `con_str`.
        '''
        # The content string has to split only at valid list element
        # separators.  These are strings matched by the respective regexp
        # matching list element separators that are not with a string that is
        # delimited by other delimiters.  Note that it is assumed that these
        # other delimiters do match; i.e. we are only count open other begin
        # delimiters.
        # - List of list elements.
        list_elems = []
        # - Number of open other begin delimiters.
        nr_open_delims = 0
        # - Current begin position.
        beg_pos = 0
        for m in re.finditer(self._list_sep_search_rx, cont_str):
            str_m = m.group(0)
            if str_m in self.oth_begs:
                # Other beg delimiter found -> increase number of open other
                # beg delimiters.
                nr_open_delims += 1
            elif str_m in self.oth_ends:
                # Other end delimiter found -> decrease number of open other
                # beg delimiters.
                nr_open_delims -= 1
            elif re.match(self._list_sep_rx, str_m) and nr_open_delims == 0:
                # Valid list element separator found -> end current list
                # element, and start a new one.
                end_pos = m.start()
                elem = cont_str[beg_pos:end_pos]
                beg_pos = m.end()
                list_elems.append(elem)
        # Last element (if there is either one at all, or after the last valid
        # list element separator).
        elem = cont_str[beg_pos:]
        list_elems.append(elem)

        return list_elems


    def get(self, section, option, *, raw=False, vars=None,
            fallback=configparser._UNSET):
        '''
        Variant of :py:meth:`get` method for
        :py:class:`configparser.ConfigParser`.

        It has the same arguments, but return a list value for an option
        `option` whose string value has list form, if list values are
        enabled.

        Please note the list values cannot be used within `var` values.

        :return: The value got.
        '''
        # Since the standard ``ConfigParser`` cannot handle real list values
        # for the `fallback` argument, we handle fallback ourselves.
        try:
            val_str = super().get(
                section, option, raw=raw, vars=vars,
                fallback=configparser._UNSET)
            val = self._as_list_maybe(val_str)
        except (configparser.NoSectionError, configparser.NoOptionError) as e:
            if not fallback == configparser._UNSET:
                val = fallback
            else:
                raise e

        return val


    def items(self, section=configparser._UNSET, raw=False, vars=None):
        '''
        Variant of :py:meth:`items` method for
        :py:class:`configparser.ConfigParser`.

        It has the same arguments, but uses a list value for an option
        `option` whose string value has list form, if list values are enabled.

        Please note the list values cannot be used within `var` values.

        :return: a list of option value pairs, if `section` is given, or the
                 original items.
        '''
        orig_items = super().items(section=section, raw=raw, vars=vars)
        if section == configparser._UNSET:
            this_items = orig_items
        else:
            this_items = [
                (o, self._as_list_maybe(v)) for (o, v) in orig_items]

        return this_items


class MissingListDelimError(configparser.Error):
    '''
    Error that at least one of the begin (if `list_beg` is not ``None``) or
    end (if `list_end` is not ``None``) delimiter for list value is missing
    for value string `val`.
    '''
    def __init__(self, val, list_beg=None, list_end=None):
        if list_beg is not None and list_end is None:
            msg = f"List begin delimiter '{list_beg}' is missing"
        elif list_beg is None and list_end is not None:
            msg = f"List end delimiter '{list_end}' is missing"
        elif list_beg is not None and list_end is not None:
            msg = f"List begin '{list_beg}' / end '{list_end}' delimiters"
            msg += ' are both missing'
        msg += f" for value\n  {val}"

        super().__init__(msg)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
