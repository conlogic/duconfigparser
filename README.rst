..
 =============================================================================
 Title          : Extensions for :py:mod:`configparser`

 Classification : reST read me file

 Author         : Dirk Ullrich

 Date           : 2022-01-20

 Description    : Read me for package ``duconfigparser``.
 =============================================================================


=========================================================
``duconfigparser``: extensions for :py:mod:`configparser`
=========================================================

This package provides extensions for the :py:mod:`configparser` module from the
Python Standard Library.

At the moment it defines a subclass :py:class:`ConfigParser` for
:py:class:`configparser.ConfigParser` from the Standard Library that can
handle list values, too: option value strings can represent list values using
custom list delimiters and a custom element separator, and when getting those
values from a :py:class:`duconfigparser.ConfigParser` instance, they are
converted into real lists.

Beside this, there is an additional universal [#uni_cont]_ mechanism to mark
any line to be continued by next lines.  For better readability, leading
whitespace of such continuation lines is ignored.

Please note that at the moment :py:class:`duconfigparser.ConfigParser` only
support reading of list values, that lists cannot be nested in such list
values, and that lists cannot (yet) used in method arguments.


.. [#uni_cont] Please remember that :py:class:`duconfigparser.ConfigParser`
               already supports a limited form of line continuation: Values
               can also span multiple lines, as long as they are indented
               deeper than the first line of the value.  But ours is more
               universal: it can be used for any line, uses an explicit
               customizable continuation marker and allows arbitrary
               indentation for continuation lines that is ignored.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
