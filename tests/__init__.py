# ============================================================================
# Title          : Extensions for :py:mod:`configparser`
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-10-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Init module for the unit test package of package ``duconfigparser``.
'''


# This turns the ``tests`` directory into a Python package.  This eases the
# discovery of tests.


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
