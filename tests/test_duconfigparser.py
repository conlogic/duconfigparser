# ============================================================================
# Title          : Extensions for :py:mod:`configparser`
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-01-20
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`duconfigparser`.
'''


import re
import unittest
import configparser

import duconfigparser


def strip_conf_opt_val(val, line_cont=None):
    '''
    :return: option value `val` with standard stripping for option values
             done, where not-``None`` string `line_cont` marks the end of a
             continued line.
    '''
    val_stripped = val.strip()
    if line_cont is not None:
        line_cont_rx = re.escape(line_cont) + r'\n\s*'
        val_stripped = re.sub(line_cont_rx, '', val_stripped)
    val_stripped = re.sub(r'\s*\n\s*', '\n', val_stripped)

    return val_stripped


class base_duconfigparser_TC(unittest.TestCase):
    '''
    Base class for :py:mod:`duconfigparser` tests.
    '''
    def setUp(self):
        # Syntax parameters for list values.
        # - No list delimiters + no proper list element separator.
        self.no_delim_no_sep_syntax = {
            'list_beg': '\n',
            'list_sep': '\n',
            'list_end': ''
        }
        # - No list delimiters + proper list element separator (=old API).
        self.no_delim_sep_syntax = {
            'list_beg': '\n',
            'list_sep': ',',
            'list_end': ''
        }
        # - list delimiters + proper list element separator (=new API).
        self.delim_sep_syntax = {
            'list_beg': '[',
            'list_sep': ',',
            'list_end': ']'
        }
        # - list delimiters + proper list element separator + other
        #   separators.
        self.delim_sep_oth_syntax = {
            'list_beg': '[',
            'list_sep': ',',
            'list_end': ']',
            'oth_begs': ['§(', '§{'],
            'oth_ends': ['}§', ')§']
        }
        # Some default names.
        # - Default section name.
        self.sect = 'sect'
        # - Default option name.
        self.opt = 'opt'


    def mk_config_str(self, val, sect=None, opt=None):
        '''
        Create a simple configuration string with one section containing a one
        option whose value is `val`.

        A not-``None`` string `sect` / `opt` is used as section / option name
        instead of the default one.

        :return: the configuration string made.
        '''
        if sect is None:
            sect = self.sect
        if opt is None:
            opt = self.opt

        conf_str = f"[{sect}]\n{opt} = {val}\n"

        return conf_str


    def prep_cfg(self, opt_val_str, sect=None, opt=None, line_cont=None,
                 **syntax):
        '''
        Prepare a :py:class:`duconfigparser.ConfigParser` using list syntax
        parameters from dictionary `syntax` with line continuation according
        to `line_cont`, and let it read a configuration string with section
        `sect`, option `opt` having value `opt_val_str`.

        For a ``None`` `sect` or `opt` defaults for the test case are used.

        :return: the prepared configuration parser object.
        '''
        list_beg = syntax.get('list_beg')
        list_sep = syntax.get('list_sep')
        list_end = syntax.get('list_end')
        oth_begs = syntax.get('oth_begs', [])
        oth_ends = syntax.get('oth_ends', [])

        conf_str = self.mk_config_str(opt_val_str, sect, opt)

        cp = duconfigparser.ConfigParser(
            list_beg=list_beg, list_sep=list_sep, list_end=list_end,
            oth_begs=oth_begs, oth_ends=oth_ends, line_cont=line_cont)
        cp.read_string(conf_str)

        return cp


class duconfigparser_line_cont_TC(base_duconfigparser_TC):
    '''
    Tests with active line continuation.
    '''


    def test_dollar_str_no_cont_line(self):
        '''
        Test with ``$`` end marker + string value without continuation lines.
        '''
        line_cont = '$'
        val = 'foo\n'
        cp = self.prep_cfg(
            val, line_cont=line_cont, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val, line_cont)
        self.assertSequenceEqual(res_val, exp_val)


    def test_dollar_list_no_cont_line(self):
        '''
        Test with ``$`` end marker + list value without continuation lines.
        '''
        line_cont = '$'
        val = '[foo, bar]'
        cp = self.prep_cfg(
            val, line_cont=line_cont, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['foo', 'bar']
        self.assertSequenceEqual(res_val, exp_val)


    def test_dollar_str_1_cont_line(self):
        '''
        Test with ``$`` end marker + string value with 1 continuation line.
        '''
        line_cont = '$'
        val = 'foo$\n bar'
        cp = self.prep_cfg(
            val, line_cont=line_cont, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val, line_cont)
        self.assertSequenceEqual(res_val, exp_val)


    def test_dollar_list_1_cont_line(self):
        '''
        Test with ``$`` end marker + list value with 1 continuation line.
        '''
        line_cont = '$'
        val = '[foo$\n bar, baz]'
        cp = self.prep_cfg(
            val, line_cont=line_cont, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['foobar', 'baz']
        self.assertSequenceEqual(res_val, exp_val)


    def test_dollar_str_2_cont_lines(self):
        '''
        Test with ``$`` end marker + string value with 2 continuation lines.
        '''
        line_cont = '$'
        val = 'foo$\n bar$\n   baz'
        cp = self.prep_cfg(
            val, line_cont=line_cont, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val, line_cont)
        self.assertSequenceEqual(res_val, exp_val)


    def test_backslash_list_2_cont_lines(self):
        '''
        Test with ``$`` end marker + string value with 2 continuation lines.
        '''
        line_cont = '\\'
        val = 'foo\\\n bar\\\n   baz'
        cp = self.prep_cfg(
            val, line_cont=line_cont, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val, line_cont)
        self.assertSequenceEqual(res_val, exp_val)


class duconfigparser_get_TC(base_duconfigparser_TC):
    '''
    Tests for method :py:meth:`duconfigparser.get`.
    '''


    def test_no_beg(self):
        '''
        Test with disabled list value support because of `list_beg`.
        '''
        val = ' [ ] '
        syntax = self.delim_sep_syntax.copy()
        syntax['list_beg'] = None
        cp = self.prep_cfg(val, **syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val)
        self.assertSequenceEqual(res_val, exp_val)


    def test_no_end(self):
        '''
        Test with disabled list value support because of `list_end`.
        '''
        val = '[foo] '
        syntax = self.delim_sep_syntax.copy()
        syntax['list_end'] = None
        cp = self.prep_cfg(val, **syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val)
        self.assertSequenceEqual(res_val, exp_val)


    def test_no_sep(self):
        '''
        Test with disabled list value support because of `list_sep`.
        '''
        val = ' [foo, bar]'
        syntax = self.delim_sep_syntax.copy()
        syntax['list_sep'] = None
        cp = self.prep_cfg(val, **syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val)
        self.assertSequenceEqual(res_val, exp_val)


    def test_miss_sect_error_no_delim_no_sep(self):
        '''
        Test with a missing section - no delimiters + no element separator.
        '''
        sect = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        with self.assertRaises(configparser.NoSectionError):
            cp.get(sect, self.opt)


    def test_miss_sect_error_no_delim_sep(self):
        '''
        Test with a missing section - no delimiters + element separator.
        '''
        sect = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        with self.assertRaises(configparser.NoSectionError):
            cp.get(sect, self.opt)


    def test_miss_sect_error_delim_sep(self):
        '''
        Test with a missing section - delimiters + element separator.
        '''
        sect = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        with self.assertRaises(configparser.NoSectionError):
            cp.get(sect, self.opt)


    def test_miss_opt_error_no_delim_no_sep(self):
        '''
        Test with a missing option - no delimiters + no element separator.
        '''
        opt = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        with self.assertRaises(configparser.NoOptionError):
            cp.get(self.sect, opt)


    def test_miss_opt_error_no_delim_sep(self):
        '''
        Test with a missing option - no delimiters + element separator.
        '''
        opt = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        with self.assertRaises(configparser.NoOptionError):
            cp.get(self.sect, opt)


    def test_miss_opt_error_delim_sep(self):
        '''
        Test with a missing option - delimiters + element separator.
        '''
        opt = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        with self.assertRaises(configparser.NoOptionError):
            cp.get(self.sect, opt)


    def test_missig_list_beg_error_delim_sep(self):
        '''
        Test with a list value but a with missing begin delimiter - delimiters
        + element separator.
        '''
        val = ']'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        with self.assertRaises(duconfigparser.MissingListDelimError):
            cp.get(self.sect, self.opt)


    def test_missig_list_end_error_delim_sep(self):
        '''
        Test with a list value but a with missing end delimiter - delimiters +
        element separator.
        '''
        val = '[ foo'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        with self.assertRaises(duconfigparser.MissingListDelimError):
            cp.get(self.sect, self.opt)


    def test_missig_list_beg_end_delim_sep(self):
        '''
        Test with a list value with list separator but a with both missing
        begin and end delimiter - delimiters + element separator.
        '''
        val = 'foo,bar'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val)
        self.assertSequenceEqual(res_val, exp_val)


    def test_miss_sect_str_fallback_no_delim_no_sep(self):
        '''
        Test with a missing section and a string fallback value - no
        delimiters + no element separator.
        '''
        sect = 'foo'
        val = 'bar'
        fallback = 'baz'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_val = cp.get(sect, self.opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_sect_str_fallback_no_delim_sep(self):
        '''
        Test with a missing section and a string fallback value - no
        delimiters + element separator.
        '''
        sect = 'foo'
        val = 'bar'
        fallback = 'baz'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_val = cp.get(sect, self.opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_sect_str_fallback_delim_sep(self):
        '''
        Test with a missing section and a string fallback value - delimiters +
        element separator.
        '''
        sect = 'foo'
        val = 'bar'
        fallback = 'baz'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(sect, self.opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_opt_str_fallback_no_delim_no_sep(self):
        '''
        Test with a missing option and a string fallback value - no delimiters
        + element separator.
        '''
        opt = 'foo'
        val = 'bar'
        fallback = 'baz'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_opt_str_fallback_no_delim_sep(self):
        '''
        Test with a missing option and a string fallback value - no delimiters
        + element separator.
        '''
        opt = 'foo'
        val = 'bar'
        fallback = 'baz'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_val = cp.get(self.sect, opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_opt_str_fallback_delim_sep(self):
        '''
        Test with a missing option and a string fallback value - delimiters +
        element separator.
        '''
        opt = 'foo'
        val = 'bar'
        fallback = 'baz'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_sect_list_fallback_no_delim_no_sep(self):
        '''
        Test with a missing section and a list fallback value - no delimiters
        + no element separator.
        '''
        sect = 'foo'
        val = 'bar'
        fallback = ['baz']
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_val = cp.get(sect, self.opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_sect_list_fallback_no_delim_sep(self):
        '''
        Test with a missing section and a list fallback value - no delimiters
        + element separator.
        '''
        sect = 'foo'
        val = 'bar'
        fallback = ['baz']
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_val = cp.get(sect, self.opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_sect_list_fallback_delim_sep(self):
        '''
        Test with a missing section and a list fallback value - delimiters +
        element separator.
        '''
        sect = 'foo'
        val = '[bar]'
        fallback = ['baz']
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(sect, self.opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_opt_list_fallback_no_delim_no_sep(self):
        '''
        Test with a missing option and a list fallback value - no delimiters +
        no element separator.
        '''
        opt = 'foo'
        val = 'bar'
        fallback = ['baz']
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_opt_list_fallback_no_delim_sep(self):
        '''
        Test with a missing option and a list fallback value - no delimiters +
        element separator.
        '''
        opt = 'foo'
        val = 'bar'
        fallback = ['baz']
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_val = cp.get(self.sect, opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_miss_opt_list_fallback_delim_sep(self):
        '''
        Test with a missing option and a list fallback value - delimiters +
        element separator.
        '''
        opt = 'foo'
        val = '[bar]'
        fallback = ['baz']
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, opt, fallback=fallback)
        self.assertSequenceEqual(res_val, fallback)


    def test_string_no_delim_no_sep(self):
        '''
        Test with a string value - no delimiters + no element separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val)
        self.assertSequenceEqual(res_val, exp_val)


    def test_string_no_delim_sep(self):
        '''
        Test with a string value - no delimiters + element separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val)
        self.assertSequenceEqual(res_val, exp_val)


    def test_string_delim_sep(self):
        '''
        Test with a string value - delimiters + element separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = strip_conf_opt_val(val)
        self.assertSequenceEqual(res_val, exp_val)


    def test_list0_delim_sep(self):
        '''
        Test with a list value without elements - delimiters + element
        separator.
        '''
        val = ' [ ] '
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = []
        self.assertSequenceEqual(res_val, exp_val)


    def test_list1_no_delim_no_sep(self):
        '''
        Test with a list value with 1 element - no delimiters + no element
        separator.
        '''
        val = '\n foo'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['foo']
        self.assertSequenceEqual(res_val, exp_val)


    def test_list1_no_delim_sep(self):
        '''
        Test with a list value with 1 element - no delimiters + element
        separator.
        '''
        val = '\n foo'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['foo']
        self.assertSequenceEqual(res_val, exp_val)


    def test_list1_delim_sep(self):
        '''
        Test with a list value with 1 element - delimiters + element separator.
        '''
        val = '[foo]'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['foo']
        self.assertSequenceEqual(res_val, exp_val)


    def test_list1_delim_sep_oth(self):
        '''
        Test with a list value with 1 element - delimiters + element separator
        + other delimiters.
        '''
        val = '[§(foo bar, baz)§]'
        cp = self.prep_cfg(val, **self.delim_sep_oth_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['§(foo bar, baz)§']
        self.assertSequenceEqual(res_val, exp_val)


    def test_list2_no_delim_no_sep(self):
        '''
        Test with a list value with 2 elements - no delimiters + no element
        separator.
        '''
        val = '\n foo\n bar'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['foo', 'bar']
        self.assertSequenceEqual(res_val, exp_val)


    def test_list2_no_delim_sep(self):
        '''
        Test with a list value with 2 elements - no delimiters + element
        separator.
        '''
        val = '\n foo,bar'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['foo', 'bar']
        self.assertSequenceEqual(res_val, exp_val)


    def test_list2_delim_sep(self):
        '''
        Test with a list value with 2 elements - delimiters + element
        separator.
        '''
        val = '[foo, bar]'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['foo', 'bar']
        self.assertSequenceEqual(res_val, exp_val)


    def test_list2_delim_sep_oth(self):
        '''
        Test with a list value with 2 elements - delimiters + element
        separator + other delimiters.
        '''
        val = '[§(out bar, §(inn one, §{two}§)§)§, §{foo.baz}§]'
        cp = self.prep_cfg(val, **self.delim_sep_oth_syntax)
        res_val = cp.get(self.sect, self.opt)
        exp_val = ['§(out bar, §(inn one, §{two}§)§)§', '§{foo.baz}§']
        self.assertSequenceEqual(res_val, exp_val)


class duconfigparser_items_TC(base_duconfigparser_TC):
    '''
    Tests for method :py:meth:`duconfigparser.items`.
    '''
    def check_all_sects(self, res_items, exp_opt_val_str,
                        sect=None, opt=None):
        '''
        Check result `res_items` for calling :py:meth:items() without argument
        for a :py:class:`duconfigparser` that has read a configuration string
        with section `sect`, option `opt` having expected value string
        `exp_opt_val_str`.

        For a ``None`` `sect` or `opt` defaults for the test case are used.
        '''
        if sect is None:
            sect = self.sect
        if opt is None:
            opt = self.opt

        # Singleton list containing the result section.
        res_sect_list = [i[1] for i in res_items if i[0] == sect]
        # The result section.
        res_sect = res_sect_list[0]
        # The result value.
        res_opt_val = res_sect[opt]

        self.assertSequenceEqual(res_opt_val, exp_opt_val_str)


    def check_one_sect(self, res_items, exp_opt_val_str, opt=None):
        '''
        Check result `res_items` for calling :py:meth:items() with a section
        argument ``sect`` for a :py:class:`duconfigparser` that has read a
        configuration string with option `opt` for ``sect`` having expected
        value string `exp_opt_val_str`.

        For a ``None`` `opt` the default one for the test case are used.
        '''
        if opt is None:
            opt = self.opt

        exp_sect_items = [(opt, exp_opt_val_str)]

        self.assertSequenceEqual(res_items, exp_sect_items)


    def test_all_no_beg(self):
        '''
        Test for all sections with disabled list value support because of
        `list_beg`.
        '''
        val = ' [ ] '
        syntax = self.delim_sep_syntax.copy()
        syntax['list_beg'] = None
        cp = self.prep_cfg(val, **syntax)
        res_items = cp.items()
        exp_val = strip_conf_opt_val(val)
        self.check_all_sects(res_items, exp_val)


    def test_all_no_end(self):
        '''
        Test for all sections with disabled list value support because of
        `list_end`.
        '''
        val = '[foo] '
        syntax = self.delim_sep_syntax.copy()
        syntax['list_end'] = None
        cp = self.prep_cfg(val, **syntax)
        res_items = cp.items()
        exp_val = strip_conf_opt_val(val)
        self.check_all_sects(res_items, exp_val)


    def test_all_no_sep(self):
        '''
        Test for all sections with disabled list value support because of
        `list_sep`.
        '''
        val = ' [foo, bar]'
        syntax = self.delim_sep_syntax.copy()
        syntax['list_sep'] = None
        cp = self.prep_cfg(val, **syntax)
        res_items = cp.items()
        exp_val = strip_conf_opt_val(val)
        self.check_all_sects(res_items, exp_val)


    def test_one_no_beg(self):
        '''
        Test for one section with disabled list value support because of
        `list_beg`.
        '''
        val = ' [ ] '
        syntax = self.delim_sep_syntax.copy()
        syntax['list_beg'] = None
        cp = self.prep_cfg(val, **syntax)
        res_items = cp.items(self.sect)
        exp_val = strip_conf_opt_val(val)
        self.check_one_sect(res_items, exp_val)


    def test_one_no_end(self):
        '''
        Test for one section with disabled list value support because of
        `list_end`.
        '''
        val = '[foo] '
        syntax = self.delim_sep_syntax.copy()
        syntax['list_end'] = None
        cp = self.prep_cfg(val, **syntax)
        res_items = cp.items(self.sect)
        exp_val = strip_conf_opt_val(val)
        self.check_one_sect(res_items, exp_val)


    def test_one_no_sep(self):
        '''
        Test for one section with disabled list value support because of
        `list_sep`.
        '''
        val = ' [foo, bar]'
        syntax = self.delim_sep_syntax.copy()
        syntax['list_sep'] = None
        cp = self.prep_cfg(val, **syntax)
        res_items = cp.items(self.sect)
        exp_val = strip_conf_opt_val(val)
        self.check_one_sect(res_items, exp_val)


    def test_one_miss_sect_error_no_delim_no_sep(self):
        '''
        Test for one section with a missing section - no delimiters + no
        element separator.
        '''
        sect = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        with self.assertRaises(configparser.NoSectionError):
            cp.items(sect)


    def test_one_miss_sect_error_no_delim_sep(self):
        '''
        Test for one section with a missing section - no delimiters + element
        separator.
        '''
        sect = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        with self.assertRaises(configparser.NoSectionError):
            cp.items(sect)


    def test_one_miss_sect_error_delim_sep(self):
        '''
        Test for one section with a missing section - delimiters + element
        separator.
        '''
        sect = 'foo'
        val = 'bar'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        with self.assertRaises(configparser.NoSectionError):
            cp.items(sect)


    def test_one_missig_list_beg_error_delim_sep(self):
        '''
        Test for one section with a list value but a with missing begin
        delimiter - delimiters + element separator.
        '''
        val = ']'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        with self.assertRaises(duconfigparser.MissingListDelimError):
            cp.items(self.sect)


    def test_one_missig_list_end_error_delim_sep(self):
        '''
        Test for one section with a list value but a with missing end
        delimiter - delimiters + element separator.
        '''
        val = '[ foo'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        with self.assertRaises(duconfigparser.MissingListDelimError):
            cp.items(self.sect)


    def test_all_missig_list_beg_end_delim_sep(self):
        '''
        Test for all sections with a list value with list separator but a with
        both missing begin and end delimiter - delimiters + element
        separator.
        '''
        val = 'foo,bar'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items()
        exp_val = strip_conf_opt_val(val)
        self.check_all_sects(res_items, exp_val)


    def test_one_missig_list_beg_end_delim_sep(self):
        '''
        Test for one section with a list value with list separator but a with
        both missing begin and end delimiter - delimiters + element
        separator.
        '''
        val = 'foo,bar'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = strip_conf_opt_val(val)
        self.check_one_sect(res_items, exp_val)


    def test_all_string_no_delim_no_sep(self):
        '''
        Test for all sections with a string value - no delimiters + no element
        separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_items = cp.items()
        exp_val = strip_conf_opt_val(val)
        self.check_all_sects(res_items, exp_val)


    def test_one_string_no_delim_no_sep(self):
        '''
        Test for one section with a string value - no delimiters + no element
        separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = strip_conf_opt_val(val)
        self.check_one_sect(res_items, exp_val)


    def test_all_string_no_delim_sep(self):
        '''
        Test for all sections with a string value - no delimiters + element
        separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_items = cp.items()
        exp_val = strip_conf_opt_val(val)
        self.check_all_sects(res_items, exp_val)


    def test_one_string_no_delim_sep(self):
        '''
        Test for one section with a string value - no delimiters + element
        separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = strip_conf_opt_val(val)
        self.check_one_sect(res_items, exp_val)


    def test_all_string_delim_sep(self):
        '''
        Test for all sections with a string value - delimiters + element
        separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items()
        exp_val = strip_conf_opt_val(val)
        self.check_all_sects(res_items, exp_val)


    def test_one_string_delim_sep(self):
        '''
        Test for one section with a string value - delimiters + element
        separator.
        '''
        val = 'foo\n'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = strip_conf_opt_val(val)
        self.check_one_sect(res_items, exp_val)


    def test_all_list0_delim_sep(self):
        '''
        Test for all sections with a list value without elements - delimiters
        + element separator.
        '''
        val = ' [ ] '
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items()
        exp_val = []
        self.check_all_sects(res_items, exp_val)


    def test_one_list0_delim_sep(self):
        '''
        Test for one section with a list value without elements - delimiters
        + element separator.
        '''
        val = ' [ ] '
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = []
        self.check_one_sect(res_items, exp_val)


    def test_all_list1_no_delim_no_sep(self):
        '''
        Test for all sections with a list value with 1 element - no delimiters
        + no element separator.
        '''
        val = '\n foo'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_items = cp.items()
        exp_val = ['foo']
        self.check_all_sects(res_items, exp_val)


    def test_one_list1_no_delim_no_sep(self):
        '''
        Test for one section with a list value with 1 element - no delimiters
        + no element separator.
        '''
        val = '\n foo'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = ['foo']
        self.check_one_sect(res_items, exp_val)


    def test_all_list1_no_delim_sep(self):
        '''
        Test for all sections with a list value with 1 element - no delimiters
        + element separator.
        '''
        val = '\n foo'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_items = cp.items()
        exp_val = ['foo']
        self.check_all_sects(res_items, exp_val)


    def test_one_list1_no_delim_sep(self):
        '''
        Test for one section with a list value with 1 element - no delimiters
        + element separator.
        '''
        val = '\n foo'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = ['foo']
        self.check_one_sect(res_items, exp_val)


    def test_all_list1_delim_sep(self):
        '''
        Test for all sections with a list value with 1 element - delimiters +
        element separator.
        '''
        val = '[foo]'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items()
        exp_val = ['foo']
        self.check_all_sects(res_items, exp_val)


    def test_all_list1_delim_sep_oth(self):
        '''
        Test for all sections with a list value with 1 element - delimiters +
        element separator + other separators.
        '''
        val = '[§(foo bar, baz)§]'
        cp = self.prep_cfg(val, **self.delim_sep_oth_syntax)
        res_items = cp.items()
        exp_val = ['§(foo bar, baz)§']
        self.check_all_sects(res_items, exp_val)


    def test_one_list1_delim_sep(self):
        '''
        Test for one section with a list value with 1 element - delimiters +
        element separator.
        '''
        val = '[foo]'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = ['foo']
        self.check_one_sect(res_items, exp_val)


    def test_one_list1_delim_sep_oth(self):
        '''
        Test for one section with a list value with 1 element - delimiters +
        element separator + other delimiters.
        '''
        val = '[§(foo bar, baz)§]'
        cp = self.prep_cfg(val, **self.delim_sep_oth_syntax)
        res_items = cp.items(self.sect)
        exp_val = ['§(foo bar, baz)§']
        self.check_one_sect(res_items, exp_val)


    def test_all_list2_no_delim_no_sep(self):
        '''
        Test for all sections with a list value with 2 elements - no
        delimiters + no element separator.
        '''
        val = '\n foo\n bar'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_items = cp.items()
        exp_val = ['foo', 'bar']
        self.check_all_sects(res_items, exp_val)


    def test_one_list2_no_delim_no_sep(self):
        '''
        Test for one section with a list value with 2 elements - no
        delimiters + no element separator.
        '''
        val = '\n foo\n bar'
        cp = self.prep_cfg(val, **self.no_delim_no_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = ['foo', 'bar']
        self.check_one_sect(res_items, exp_val)


    def test_all_list2_no_delim_sep(self):
        '''
        Test for all sections with a list value with 2 elements - no
        delimiters + element separator.
        '''
        val = '\n foo,bar'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_items = cp.items()
        exp_val = ['foo', 'bar']
        self.check_all_sects(res_items, exp_val)


    def test_one_list2_no_delim_sep(self):
        '''
        Test for one section with a list value with 2 elements - no
        delimiters + element separator.
        '''
        val = '\n foo,bar'
        cp = self.prep_cfg(val, **self.no_delim_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = ['foo', 'bar']
        self.check_one_sect(res_items, exp_val)


    def test_all_list2_delim_sep(self):
        '''
        Test for all sections with a list value with 2 elements - delimiters +
        element separator.
        '''
        val = '[foo, bar]'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items()
        exp_val = ['foo', 'bar']
        self.check_all_sects(res_items, exp_val)


    def test_all_list2_delim_sep_oth(self):
        '''
        Test for all sections with a list value with 2 elements - delimiters +
        element separator + other delimiters.
        '''
        val = '[§(out bar, §(inn one, §{two}§)§)§, §{foo.baz}§]'
        cp = self.prep_cfg(val, **self.delim_sep_oth_syntax)
        res_items = cp.items()
        exp_val = ['§(out bar, §(inn one, §{two}§)§)§', '§{foo.baz}§']
        self.check_all_sects(res_items, exp_val)


    def test_one_list2_delim_sep(self):
        '''
        Test for one section with a list value with 2 elements - delimiters +
        element separator.
        '''
        val = '[foo, bar]'
        cp = self.prep_cfg(val, **self.delim_sep_syntax)
        res_items = cp.items(self.sect)
        exp_val = ['foo', 'bar']
        self.check_one_sect(res_items, exp_val)


    def test_one_list2_delim_sep_oth(self):
        '''
        Test for one section with a list value with 2 elements - delimiters +
        element separator + other delimiters.
        '''
        val = '[§(out bar, §(inn one, §{two}§)§)§, §{foo.baz}§]'
        cp = self.prep_cfg(val, **self.delim_sep_oth_syntax)
        res_items = cp.items(self.sect)
        exp_val = ['§(out bar, §(inn one, §{two}§)§)§', '§{foo.baz}§']
        self.check_one_sect(res_items, exp_val)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
